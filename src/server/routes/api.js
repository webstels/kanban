import { Router } from "express";
import {normalize, schema} from "normalizr";

const api = db => {
  const router = Router();
  const boards = db.collection("boards");
  const users = db.collection("users");

  const normalizeBoards = nBoards => {
    const card = new schema.Entity("cardsById", {}, { idAttribute: "_id" });
    const row = new schema.Entity("rowsById", {}, { idAttribute: "_id" });
    const list = new schema.Entity(
        "listsById",
        { cards: [card], rows: [row] },
        { idAttribute: "_id" }
    );
    const board = new schema.Entity(
        "boardsById",
        { lists: [list] },
        { idAttribute: "_id" }
    );
    const { entities } = normalize(nBoards, [board]);
    return entities;
  };

  // Replace the entire board every time the users modifies it in any way.
  // This solution sends more data than necessary, but cuts down on code and
  // effectively prevents the db and client from ever getting out of sync
  router.put("/board", (req, res) => {
    const board = req.body;
   boards
      .replaceOne({ _id: board._id, users: req.user._id }, board, {
        upsert: true
      })
      .then(result => {
        res.send(result);
      });
  });

  router.delete("/board", (req, res) => {
    const { boardId } = req.body;
    boards.deleteOne({ _id: boardId }).then(result => {
      res.send(result);
    });
  });

  router.post("/checkUser", (req, res) => {
    const { userId } = req.body;
    users.findOne({ _id: userId }).then(result => {
      if(result !== null) {
        res.json({result: true});
      } else {
        res.json({result: false});
      }
    });
  });

  router.get("/getAllData", (req, res, next) => {
    if (req.user) {
      const collection = db.collection("boards");
      collection
          .find({ $or: [{ users: req.user._id }, { isPublic: true }] })
          .toArray()
          .then(rBoards => {
            res.json({ ...normalizeBoards(rBoards), user: req.user });
            /*req.initialState = { ...normalizeBoards(rBoards), user: req.user };
            next();*/
          });

      // Just create the welcome board if no user is logged in
    }
  });

  router.get("/fetchUsersDataByIds", (req, res, next) => {
    if (req.user) {
      const collection = db.collection("users");
      collection
          .find({ $or: [{ _id: [req.users] }, { isPublic: true }] })
          .toArray()
          .then(users => {
            res.json(users);
            /*req.initialState = { ...normalizeBoards(rBoards), user: req.user };
            next();*/
          });

      // Just create the welcome board if no user is logged in
    }
  });

  return router;
};

export default api;
