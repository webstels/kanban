const cardsById = (state = {}, action) => {
  switch (action.type) {
    case "ADD_CARD": {
      const { cardText, cardId } = action.payload;
      return { ...state, [cardId]: { text: cardText, _id: cardId } };
    }
    case "CHANGE_CARD_TEXT": {
      const { cardText, cardId } = action.payload;
      return { ...state, [cardId]: { ...state[cardId], text: cardText } };
    }
    case "CHANGE_CARD_DATE": {
      const { date, cardId } = action.payload;
      return { ...state, [cardId]: { ...state[cardId], date } };
    }
    case "CHANGE_CARD_COLOR": {
      const { color, cardId } = action.payload;
      return { ...state, [cardId]: { ...state[cardId], color } };
    }
    case "DELETE_CARD": {
      const { cardId } = action.payload;
      const { [cardId]: deletedCard, ...restOfCards } = state;
      return restOfCards;
    }
    // Find every card from the deleted list and remove it (actually unnecessary since they will be removed from db on next write anyway)
    case "DELETE_LIST": {
      const { cards: cardIds } = action.payload;
      return Object.keys(state)
        .filter(cardId => !cardIds.includes(cardId))
        .reduce(
          (newState, cardId) => ({ ...newState, [cardId]: state[cardId] }),
          {}
        );
    }
    case "REFRESH_DATA": {
      const { cards } = action.payload;
      const newState = {... state};
      Object.keys(cards).forEach((cardId) => {
        newState[cardId] = {
          _id: cards[cardId]._id,
          text: cards[cardId].text
        }
        if(cards[cardId].hasOwnProperty('date')){
          newState[cardId].date = cards[cardId].date;
        }
        if(cards[cardId].hasOwnProperty('color')){
          newState[cardId].color = cards[cardId].color;
        }
      });
      return newState;
    }
    default:
      return state;
  }
};

export default cardsById;
