import { combineReducers } from "redux";
import { snackbarReducer } from "react-redux-snackbar";
import cardsById from "./cardsById";
import rowsById from "./rowsById";
import listsById from "./listsById";
import boardsById from "./boardsById";
import user from "./user";
import isGuest from "./isGuest";
import currentBoardId from "./currentBoardId";

export default combineReducers({
  cardsById,
  listsById,
  boardsById,
  user,
  isGuest,
  currentBoardId,
  rowsById,
  snackbar: snackbarReducer
});
