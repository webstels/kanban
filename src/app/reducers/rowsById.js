const rowsById = (state = {}, action) => {
  switch (action.type) {
    case "ADD_ROW": {
      const { rowText, rowId } = action.payload;
      return { ...state, [rowId]: { title: rowText, _id: rowId } };
    }
    default:
      return state;
  }
};

export default rowsById;
