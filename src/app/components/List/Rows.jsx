import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Droppable } from "react-beautiful-dnd";
import Row from "../Row/Row";

class Rows extends Component {
  static propTypes = {
    listId: PropTypes.string.isRequired,
    rows: PropTypes.arrayOf(PropTypes.string).isRequired,
    listWIP: PropTypes.number,
    listLength: PropTypes.number
  };

  componentDidUpdate = prevProps => {
    // Scroll to bottom of list if a new row has been added
    if (
        this.props.rows[this.props.rows.length - 2] ===
        prevProps.rows[prevProps.rows.length - 1]
    ) {
      this.scrollToBottom();
    }
  };

  scrollToBottom = () => {
    this.listEnd.scrollIntoView();
  };

  render() {
    const { listId, rows, listWIP, listLength } = this.props;
    return (
        <Droppable droppableId={listId}
                   listWIP={listWIP}
                   listLength={listLength}>
          {(provided, { isDraggingOver }) => (
              <>
                <div className="rows" ref={provided.innerRef}>
                  {rows.map((rowId, index) => (
                      <Row
                          isDraggingOver={isDraggingOver}
                          key={rowId}
                          rowId={rowId}
                          index={index}
                          listId={listId}
                      />
                  ))}
                  {provided.placeholder}
                  <div
                      style={{ float: "left", clear: "both" }}
                      ref={el => {
                        this.listEnd = el;
                      }}
                  />
                </div>
              </>
          )}
        </Droppable>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  rows: state.listsById[ownProps.listId].rows
});

export default connect(mapStateToProps)(Rows);
