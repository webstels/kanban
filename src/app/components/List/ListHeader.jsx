import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import Textarea from "react-textarea-autosize";
import {Button, Wrapper, Menu, MenuItem} from "react-aria-menubutton";
import {OutlinedInput, Icon} from "@material-ui/core"
import {FaEllipsisV, FaTrash} from 'react-icons/fa';
import {socket} from "../../middleware/socketIOReduxMiddleware";
import "./ListHeader.scss";

class ListTitle extends Component {
    static propTypes = {
        listTitle: PropTypes.string.isRequired,
        listId: PropTypes.string.isRequired,
        boardId: PropTypes.string.isRequired,
        cards: PropTypes.arrayOf(PropTypes.string).isRequired,
        dragHandleProps: PropTypes.object.isRequired,
        dispatch: PropTypes.func.isRequired,
        WIP: PropTypes.number,
        listLength: PropTypes.number
    };

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            newTitle: props.listTitle,
            newWIP: props.WIP
        };
    }

    setWIP = () => {
        const {newWIP} = this.state;
        const {listId, dispatch, boardId} = this.props;
        if (newWIP !== undefined && newWIP !== null && newWIP !== 0 && newWIP !== 'Infinity' && newWIP !== Infinity && newWIP !== '∞') {
             dispatch({
              type: "SET_WIP",
              payload: {listId, WIP: Math.floor(newWIP), boardId}
            });
                // socket.emit('SET_WIP', {listId, WIP: parseInt(newWIP, 0), boardId});
        }
    };

    handleInputChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleKeyDown = event => {
        if (event.keyCode === 13) {
            event.preventDefault();
            this.handleSubmit();
        } else if (event.keyCode === 27) {
            this.revertTitle();
        }
    };

    handleSubmit = () => {
        const {newTitle} = this.state;
        const {listTitle, listId, dispatch, boardId} = this.props;
        if (newTitle === "") return;
        if (newTitle !== listTitle) {
             dispatch({
               type: "CHANGE_LIST_TITLE",
               payload: { listTitle: newTitle, listId, boardId }
             });
                //socket.emit('CHANGE_LIST_TITLE', {listTitle: newTitle, listId, boardId});
        }
        this.setState({isOpen: false});
    };

    revertTitle = () => {
        this.setState({newTitle: this.props.listTitle, isOpen: false});
    };

    deleteList = () => {
        const {listId, cards, boardId, dispatch} = this.props;
         dispatch({
          type: "DELETE_LIST",
          payload: { cards, listId, boardId }
        });
            //socket.emit('DELETE_LIST', {cards, listId, boardId});
    };

    openTitleEditor = () => {
        this.setState({isOpen: true});
    };

    handleButtonKeyDown = event => {
        if (event.keyCode === 13) {
            event.preventDefault();
            this.openTitleEditor();
        }
    };

    render() {
        const {isOpen, newTitle, newWIP} = this.state;
        const {dragHandleProps, listTitle, WIP, listLength} = this.props;
        return (
            <div className="list-header">
                {isOpen ? (
                    <div className="list-title-textarea-wrapper">
            <Textarea
                autoFocus
                useCacheForDOMMeasurements
                value={newTitle}
                onChange={this.handleInputChange}
                onKeyDown={this.handleKeyDown}
                className="list-title-textarea"
                onBlur={this.handleSubmit}
                spellCheck={false}
                name="newTitle"
            />
                    </div>
                ) : (
                    <div
                        {...dragHandleProps}
                        role="button"
                        tabIndex={0}
                        onClick={this.openTitleEditor}
                        onKeyDown={event => {
                            this.handleButtonKeyDown(event);
                            dragHandleProps.onKeyDown(event);
                        }}
                        className="list-title-button"
                    >
                        {listTitle}
                    </div>
                )}
                <Wrapper className="setWIP-list-wrapper">
                    <Button className="setWIP-list-button">
                        {listLength}
                        <FaEllipsisV/>
                        {WIP === 0 ? '∞' : WIP}
                    </Button>
                    <Menu className="setWIP-list-menu">
                        <div className="setWIP-list-header"><OutlinedInput type="number" name="newWIP"
                                                                   onChange={this.handleInputChange}
                                                                   value={newWIP === 0 ? '∞' : newWIP}/></div>
                        <MenuItem className="setWIP-list-confirm" onClick={this.setWIP}>Установить WIP</MenuItem>
                    </Menu>
                </Wrapper>
                <Wrapper className="delete-list-wrapper" onSelection={this.deleteList}>
                    <Button className="delete-list-button">
                       <Icon>delete</Icon>
                    </Button>
                    <Menu className="delete-list-menu">
                        <div className="delete-list-header">Вы уверены?</div>
                        <MenuItem className="delete-list-confirm">Удалить</MenuItem>
                    </Menu>
                </Wrapper>
            </div>
        );
    }
}

export default connect()(ListTitle);
