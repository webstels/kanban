import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Draggable } from "react-beautiful-dnd";
import classnames from "classnames";
import ListHeader from "./ListHeader";
import Cards from "./Cards";
import CardAdder from "../CardAdder/CardAdder";
import "./List.scss";

class List extends Component {
  static propTypes = {
    boardId: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
    list: PropTypes.shape({ _id: PropTypes.string.isRequired }).isRequired,
  };

  render = () => {
    const { list, boardId, index } = this.props;
    let cardAdder;
    if (list.cards.length < list.WIP || list.WIP === undefined || list.WIP === 0) {
      cardAdder = <CardAdder boardId={boardId} listId={list._id} listWIP={list.WIP} listLength={list.cards.length}/>;
    }
    return (
      <Draggable
        draggableId={list._id}
        index={index}
        listWIP={list.WIP}
        boardId={boardId}
        listLength={list.cards.length}
        disableInteractiveElementBlocking
      >
        {(provided, snapshot) => (
          <>
            <div
              ref={provided.innerRef}
              {...provided.draggableProps}
              className="list-wrapper"
            >
              <div
                className={classnames("list", {
                  "list--drag": snapshot.isDragging
                })}
              >
                <ListHeader
                  dragHandleProps={provided.dragHandleProps}
                  listTitle={list.title}
                  listId={list._id}
                  cards={list.cards}
                  rows={list.rows}
                  boardId={boardId}
                  WIP={list.WIP}
                  listLength={list.cards.length}
                />
                <div className="cards-wrapper">
                  <Cards boardId={boardId} listId={list._id} listWIP={list.WIP} listLength={list.cards.length} />
                </div>
                {/*<Rows listId={list._id} listWIP={list.WIP} listLength={list.cards.length} />*/}
              </div>
              {cardAdder}
              {/*<CardRowAdder listId={list._id} listWIP={list.WIP} listLength={list.cards.length}/>*/}
            </div>
            {provided.placeholder}
          </>
        )}
      </Draggable>

    );
  };
}

export default connect()(List);
