import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { FaUserSecret, FaSignOutAlt, FaSignInAlt} from "react-icons/fa";
import Icon from "@material-ui/core/Icon";
import kanbanLogo from "../../../assets/images/logo.png";
import "./Header.scss";
import ClickOutside from "../ClickOutside/ClickOutside";

class Header extends Component {
  static propTypes = { user: PropTypes.object };

    constructor(props) {
        super(props);
        this.state = {
            isUserProfileOpen: false,
        };
    }

    showUserId = () => {
        const { user } = this.props;
        this.setState({ isUserProfileOpen: true });
  };
    toggleUserProfile = () => {
            this.setState({ isUserProfileOpen: false });
    };
  render = () => {
    const { user } = this.props;
    const { isUserProfileOpen } = this.state;
    return (
      <header>
        <Link to="/" className="header-title">
          <img src={kanbanLogo}/>
        </Link>
        <div className="header-right-side">
          {user ? (
            <img onClick={_ => {
                this.showUserId();
            }}
              src={user.imageUrl}
              alt={user.name}
              className="user-thumbnail"
              title={user.name}
            />
          ) : (
            <FaUserSecret className="guest-icon" />
          )}
            {isUserProfileOpen ? (
                <ClickOutside handleClickOutside={this.toggleUserProfile}>
                <div className="userProfile">
                    <div><strong>{user.name}</strong> <br/> [{user._id}]</div>
                </div>
                </ClickOutside>
            ) : ( null )}
          {user ? (
            <a className="signout-link" href="/auth/signout">
                <Icon>exit_to_app</Icon>
              &nbsp;Выйти
            </a>
          ) : (
            <a className="signout-link" href="/">
              <FaSignInAlt className="signout-icon" />
              &nbsp;Войти
            </a>
          )}
        </div>
      </header>
    );
  };
}

const mapStateToProps = ({ user }) => ({ user });

export default connect(mapStateToProps)(Header);
