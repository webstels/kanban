import React, {Component} from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types"
import { Snackbar, showSnack } from 'react-redux-snackbar';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import {socket} from "../../middleware/socketIOReduxMiddleware";
import "./Move.scss";


class Move extends Component {
    static propTypes = {
        // dispatch: PropTypes.func.isRequired,
        boards: PropTypes.arrayOf(
            PropTypes.shape({
                _id: PropTypes.string.isRequired,
                color: PropTypes.string.isRequired,
                title: PropTypes.string.isRequired
            }).isRequired
        ).isRequired,
        listsById: PropTypes.object.isRequired,
        boardId: PropTypes.string.isRequired,
        cardId: PropTypes.string.isRequired,
        listId: PropTypes.string.isRequired,
        toggleMove: PropTypes.func.isRequired,
        dispatch: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        const selectedBoard = props.boards.filter(board => board._id === props.boardId);
        let cardIndex;
        props.listsById[props.listId].cards.forEach((card, index) => {
           if(card === props.cardId){
               cardIndex = index;
           }
        });
        this.state = {selectedBoard: selectedBoard[0], selectedListId: props.listId, currentCardIndex: cardIndex};
    }

    changeSelectedBoard = (e) => {
        const {value} = e.target;
        const {boards} = this.props;
        const selectedBoard = boards.filter(board => board._id === value);
        this.setState({selectedBoard: selectedBoard[0], selectedListId: selectedBoard[0].lists[0]});
    };

    changeSelectedListId = (e) => {
        const {value} = e.target;
        this.setState({selectedListId: value});
    };

    handleSave = () => {
        const {selectedListId, currentCardIndex} = this.state;
        const {listId, dispatch, toggleMove, boards} = this.props;
        const sourceBoard = boards.filter(board => board.lists.indexOf(listId) !== -1)[0];
        const destBoard = boards.filter(board => board.lists.indexOf(selectedListId) !== -1)[0];
          dispatch({
          type: "MOVE_CARD_ANOTHER_BOARD",
          payload: {destListId: selectedListId, sourceListId: listId, oldCardIndex: currentCardIndex, sourceBoardId: sourceBoard._id, destBoardID: destBoard._id}
        });
        dispatch(showSnack('move_card', {
            label: 'Карточка успешно перемещена.',
            timeout: 5000,
            button: {label: 'OK'}
        }));
        //socket.emit('MOVE_CARD_ANOTHER_BOARD', {destListId: selectedListId, sourceListId: listId, oldCardIndex: currentCardIndex});
        toggleMove();
    };


    render() {
        const {toggleMove, boards, listsById} = this.props;
        const {selectedBoard, selectedListId} = this.state;
        return (
            <div className="move">
                <FormControl className="select">
                    <InputLabel shrink htmlFor="age-label-placeholder">
                        Доска
                    </InputLabel>
                <Select
                    value={selectedBoard._id}
                    onChange={this.changeSelectedBoard}
                >
                    {boards.map(board => (
                        <MenuItem key={board._id} value={board._id}>{board.title}</MenuItem>
                    ))}

                </Select>
                </FormControl>
                <FormControl className="select">
                    <InputLabel shrink htmlFor="listId-simple">
                        Столбец
                    </InputLabel>
                <Select
                    value={selectedListId}
                    onChange={this.changeSelectedListId}
                    inputProps={{
                        name: 'listId',
                        id: 'listId-simple',
                    }}
                >
                    {selectedBoard.lists.map(eachListId => (
                        <MenuItem  key={listsById[eachListId]._id}
                                value={listsById[eachListId]._id}>{listsById[eachListId].title}</MenuItem>
                    ))}

                </Select>
                </FormControl>

                <div className="move-buttons">
                    <Button onClick={this.handleSave} variant="contained" className="save-button" color="primary">
                        Переместить
                    </Button>
                    <Button onClick={toggleMove} className="cancel-button" variant="contained">
                        Отмена
                    </Button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({boardsById, listsById}) => ({
    boards: Object.keys(boardsById).map(key => boardsById[key]),
    listsById
});

export default connect(mapStateToProps)(Move);
