import React, {Component} from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import DayPicker from "react-day-picker";
import "./ReactDayPicker.css";
import {socket} from "../../middleware/socketIOReduxMiddleware";

const WEEKDAYS_SHORT = {
    ru: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
};
const MONTHS = {
    ru: [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь',
    ]
};

const WEEKDAYS_LONG = {
    ru: [
        'Воскресенье',
        'Понедельник',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Суббота',
    ]
};

const FIRST_DAY_OF_WEEK = {
    ru: 1,
    it: 1,
};
// Translate aria-labels
const LABELS = {
    ru: {nextMonth: 'Следующий месяц', previousMonth: 'Предыдущий месяц'},
};

class Calendar extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        boardId: PropTypes.string.isRequired,
        cardId: PropTypes.string.isRequired,
        date: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
        toggleCalendar: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            selectedDay: props.date ? new Date(props.date) : undefined,
            locale: 'ru',
        };
    }

    handleDayClick = (selectedDay, {selected, disabled}) => {
        if (disabled) {
            return;
        }
        if (selected) {
            // Unselect the day if already selected
            this.setState({selectedDay: undefined});
            return;
        }
        this.setState({selectedDay});
    };

    handleSave = () => {
        const {selectedDay} = this.state;
        const {cardId, toggleCalendar, dispatch, boardId} = this.props;
        dispatch({
          type: "CHANGE_CARD_DATE",
          payload: { date: selectedDay, cardId, boardId }
        });
            // socket.emit('CHANGE_CARD_DATE', {date: selectedDay, cardId, boardId});
        toggleCalendar();
    };

    render() {
        const {selectedDay, locale} = this.state;
        const {toggleCalendar} = this.props;
        return (
            <div className="calendar">
                <DayPicker
                    locale={locale}
                    months={MONTHS[locale]}
                    weekdaysLong={WEEKDAYS_LONG[locale]}
                    weekdaysShort={WEEKDAYS_SHORT[locale]}
                    firstDayOfWeek={FIRST_DAY_OF_WEEK[locale]}
                    labels={LABELS[locale]}
                    onDayClick={this.handleDayClick}
                    selectedDays={selectedDay}
                    disabledDays={{before: new Date()}}
                />
                <div className="calendar-buttons">
                    <button onClick={this.handleSave} className="calendar-save-button">
                        Сохранить
                    </button>
                    <button onClick={toggleCalendar}>Отмена</button>
                </div>
            </div>
        );
    }
}

export default connect()(Calendar);
