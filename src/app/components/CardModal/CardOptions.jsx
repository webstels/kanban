import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Modal from "react-modal";
import {Icon} from "@material-ui/core";
import Calendar from "./Calendar";
import ClickOutside from "../ClickOutside/ClickOutside";
import colorIcon from "../../../assets/images/color-icon.png";
import "./CardOptions.scss";
import {socket} from "../../middleware/socketIOReduxMiddleware";
import Move from "./Move";
import AssignResponsible from "./AssignResponsible";

class CardOptions extends Component {
  static propTypes = {
    isColorPickerOpen: PropTypes.bool.isRequired,
    card: PropTypes.shape({ _id: PropTypes.string.isRequired }).isRequired,
    listId: PropTypes.string.isRequired,
    isCardNearRightBorder: PropTypes.bool.isRequired,
    isThinDisplay: PropTypes.bool.isRequired,
    boundingRect: PropTypes.object.isRequired,
    toggleColorPicker: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired,
    boardId: PropTypes.string.isRequired,
  };

  constructor() {
    super();
    this.state = { isCalendarOpen: false, isMoveOpen: false, isAssignResponsible: false };
  }

  deleteCard = () => {
    const { listId, card, boardId, dispatch } = this.props;
   dispatch({
      type: "DELETE_CARD",
      payload: { cardId: card._id, listId, boardId }
    });
      //socket.emit('DELETE_CARD', {cardId: card._id, listId, boardId});
  };

  changeColor = color => {
    const { dispatch, card, toggleColorPicker, boardId } = this.props;
    if (card.color !== color) {
      dispatch({
        type: "CHANGE_CARD_COLOR",
        payload: { color, cardId: card._id, boardId}
      });
        //socket.emit('CHANGE_CARD_COLOR', {color, cardId: card._id, boardId});
    }
    toggleColorPicker();
    this.colorPickerButton.focus();
  };

  handleKeyDown = event => {
    if (event.keyCode === 27) {
      this.props.toggleColorPicker();
      this.colorPickerButton.focus();
    }
  };

  handleClickOutside = () => {
    const { toggleColorPicker } = this.props;
    toggleColorPicker();
    this.colorPickerButton.focus();
  };

  toggleCalendar = () => {
    this.setState({ isCalendarOpen: !this.state.isCalendarOpen });
  };

  toggleMove = () => {
    this.setState({ isMoveOpen: !this.state.isMoveOpen });
  };

  toggleAssignResponsible = () => {
    this.setState({ isAssignResponsible: !this.state.isAssignResponsible });
  }

  render() {
    const {
      isCardNearRightBorder,
      isColorPickerOpen,
      toggleColorPicker,
      card,
      isThinDisplay,
      boundingRect,
      boardId,
        listId
    } = this.props;
    const { isCalendarOpen, isMoveOpen, isAssignResponsible } = this.state;

    const calendarStyle = {
      content: {
        top: Math.min(boundingRect.bottom + 10, window.innerHeight - 300),
      }
    };
    const moveStyle = {
      content: {
        top: Math.min(boundingRect.bottom + 10, window.innerHeight - 300),
      }
    };

    const calendarMobileStyle = {
      content: {
        top: 110,
        left: "50%",
        right: "-30%",
        transform: "translateX(-50%)"
      }
    };
    const moveMobileStyle = {
      content: {
        top: 110,
        left: "50%",
        right: "-30%",
        transform: "translateX(-50%)"
      }
    };
    return (
      <div
        className="options-list"
        style={{
          alignItems: isCardNearRightBorder ? "flex-end" : "flex-start"
        }}
      >
        <div>
          <button onClick={this.deleteCard} className="options-list-button">
            <div className="modal-icon">
              <Icon>delete</Icon>
            </div>&nbsp;Удалить
          </button>
        </div>
        <div className="modal-color-picker-wrapper">
          <button
            className="options-list-button"
            onClick={toggleColorPicker}
            onKeyDown={this.handleKeyDown}
            ref={ref => {
              this.colorPickerButton = ref;
            }}
            aria-haspopup
            aria-expanded={isColorPickerOpen}
          >
            <img src={colorIcon} alt="colorwheel" className="modal-icon" />
            &nbsp;Цвет
          </button>
          {isColorPickerOpen && (
            <ClickOutside
              eventTypes="click"
              handleClickOutside={this.handleClickOutside}
            >
              {/* eslint-disable */}
              <div
                className="modal-color-picker"
                onKeyDown={this.handleKeyDown}
              >
                {/* eslint-enable */}
                {["rgba(255, 255, 255, 0.85)", "#6df", "#6f6", "#ff6", "#fa4", "#f66"].map(
                  color => (
                    <button
                      key={color}
                      style={{ background: color }}
                      className="color-picker-color"
                      onClick={() => this.changeColor(color)}
                    />
                  )
                )}
              </div>
            </ClickOutside>
          )}
        </div>
        <div>
          <button onClick={this.toggleCalendar} className="options-list-button">
            <div className="modal-icon">
              <Icon>date_range</Icon>
            </div>&nbsp;Установить дату
          </button>
        </div>
        <Modal
          isOpen={isCalendarOpen}
          onRequestClose={this.toggleCalendar}
          overlayClassName="calendar-underlay"
          className="calendar-modal"
          style={isThinDisplay ? calendarMobileStyle : calendarStyle}
        >
          <Calendar
              boardId={boardId}
            cardId={card._id}
            date={card.date}
            toggleCalendar={this.toggleCalendar}
          />
        </Modal>
        <div>
          <button onClick={this.toggleMove} className="options-list-button">
            <div className="modal-icon">
              <Icon>open_with</Icon>
            </div>&nbsp;Переместить карточку
          </button>
        </div>
        <Modal
            isOpen={isMoveOpen}
            onRequestClose={this.toggleMove}
            overlayClassName="move-underlay"
            className="move-modal"
            style={isThinDisplay ? moveMobileStyle : moveStyle}
        >
          <Move
              listId={listId}
              boardId={boardId}
              card={card}
              cardId={card._id}
              toggleMove={this.toggleMove}
          />
        </Modal>
        <div>
          <button onClick={this.toggleAssignResponsible} className="options-list-button">
            <div className="modal-icon">
              <Icon>face</Icon>
            </div>&nbsp;Назначить ответственного
          </button>
        </div>
        <Modal
            isOpen={isAssignResponsible}
            onRequestClose={this.toggleAssignResponsible}
            overlayClassName="move-underlay"
            className="move-modal"
            style={isThinDisplay ? moveMobileStyle : moveStyle}
        >
          <AssignResponsible
              listId={listId}
              boardId={boardId}
              card={card}
              cardId={card._id}
              toggleAssignResponsible={this.toggleAssignResponsible}
          />
        </Modal>
      </div>
    );
  }
}

export default connect()(CardOptions);
