import React, {Component} from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types"
import {Snackbar, showSnack} from 'react-redux-snackbar';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import "./AssignResponsible.scss";


class AssignResponsible extends Component {
    static propTypes = {
        // dispatch: PropTypes.func.isRequired,
        boards: PropTypes.arrayOf(
            PropTypes.shape({
                _id: PropTypes.string.isRequired,
                color: PropTypes.string.isRequired,
                title: PropTypes.string.isRequired
            }).isRequired
        ).isRequired,
        listsById: PropTypes.object.isRequired,
        boardId: PropTypes.string.isRequired,
        cardId: PropTypes.string.isRequired,
        listId: PropTypes.string.isRequired,
        toggleAssignResponsible: PropTypes.func.isRequired,
        dispatch: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        const currentBoard = props.boards.filter(board => board._id === props.boardId)[0];



        this.state = {selectedResponsible: null, currentBoard};
    }

    changeSelectedResponsible = (e) => {
        const {value} = e.target;
        this.setState({selectedResponsible: value});
    };

    handleSave = () => {
        const {selectedListId, currentCardIndex} = this.state;
        const {listId, dispatch, toggleAssignResponsible, boards} = this.props;
        const sourceBoard = boards.filter(board => board.lists.indexOf(listId) !== -1)[0];
        const destBoard = boards.filter(board => board.lists.indexOf(selectedListId) !== -1)[0];
        dispatch({
            type: "MOVE_CARD_ANOTHER_BOARD",
            payload: {
                destListId: selectedListId,
                sourceListId: listId,
                oldCardIndex: currentCardIndex,
                sourceBoardId: sourceBoard._id,
                destBoardID: destBoard._id
            }
        });
        dispatch(showSnack('move_card', {
            label: 'Карточка успешно перемещена.',
            timeout: 5000,
            button: {label: 'OK'}
        }));
        //socket.emit('MOVE_CARD_ANOTHER_BOARD', {destListId: selectedListId, sourceListId: listId, oldCardIndex: currentCardIndex});
        toggleAssignResponsible();
    };


    render() {
        const {toggleAssignResponsible} = this.props;
        const {selectedResponsible, currentBoard} = this.state;
        return (
            <div className="assignResponsible">
                <FormControl className="select responsible-select">
                    <InputLabel shrink htmlFor="age-label-placeholder">
                        Ответственный
                    </InputLabel>
                    <Select
                        value={selectedResponsible}
                        onChange={this.changeSelectedResponsible}
                    >
                        {currentBoard.users.map(user => (
                            <MenuItem key={user} value={user}>{user}</MenuItem>
                        ))}

                    </Select>
                </FormControl>

                <div className="move-buttons">
                    <Button onClick={this.handleSave} variant="contained" className="save-button" color="primary">
                        Сохранить
                    </Button>
                    <Button onClick={toggleAssignResponsible} className="cancel-button" variant="contained">
                        Отмена
                    </Button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({boardsById, listsById}) => ({
    boards: Object.keys(boardsById).map(key => boardsById[key]),
    listsById
});

export default connect(mapStateToProps)(AssignResponsible);
