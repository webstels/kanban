import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Textarea from "react-textarea-autosize";
import Button from '@material-ui/core/Button';
import Modal from "react-modal";
import CardBadges from "../CardBadges/CardBadges";
import CardOptions from "./CardOptions";
import { findCheckboxes } from "../utils";
import "./CardModal.scss";
import {socket} from "../../middleware/socketIOReduxMiddleware";


class CardModal extends Component {
  static propTypes = {
    card: PropTypes.shape({
      text: PropTypes.string.isRequired,
      _id: PropTypes.string.isRequired,
      date: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
      color: PropTypes.string
    }).isRequired,
    listId: PropTypes.string.isRequired,
    boardId: PropTypes.string.isRequired,
    cardElement: PropTypes.shape({
      getBoundingClientRect: PropTypes.func.isRequired
    }),
    isOpen: PropTypes.bool.isRequired,
    toggleCardEditor: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      newText: props.card.text,
      isColorPickerOpen: false,
      isTextareaFocused: true
    };
    if (typeof document !== "undefined") {
      Modal.setAppElement("#app");
    }
  }

  componentWillReceiveProps = nextProps => {
    this.setState({ newText: nextProps.card.text });
  };

  deleteCard = () => {
    const { listId, card, dispatch, boardId } = this.props;
     dispatch({
       type: "DELETE_CARD",
       payload: { cardId: card._id, listId, boardId }
     });
    //socket.emit('DELETE_CARD', {cardId: card._id, listId, boardId});
  };

  handleKeyDown = event => {
    if (event.keyCode === 13 && event.shiftKey === false) {
      event.preventDefault();
      this.submitCard();
    }
  };

  submitCard = () => {
    const { newText } = this.state;
    const { card, listId, dispatch, toggleCardEditor, boardId } = this.props;
    if (newText === "") {
      this.deleteCard();
    } else if (newText !== card.text) {
       dispatch({
        type: "CHANGE_CARD_TEXT",
        payload: {
          cardText: newText,
          cardId: card._id,
          listId,
          boardId
        }
      });
     /* socket.emit('CHANGE_CARD_TEXT', {
        cardText: newText,
        cardId: card._id,
        listId,
        boardId
      });*/
    }
    toggleCardEditor();
  };

  handleChange = event => {
    this.setState({ newText: event.target.value });
  };

  toggleColorPicker = () => {
    this.setState({ isColorPickerOpen: !this.state.isColorPickerOpen });
  };

  handleRequestClose = () => {
    const { isColorPickerOpen } = this.state;
    const { toggleCardEditor } = this.props;
    if (!isColorPickerOpen) {
      toggleCardEditor();
      //this.submitCard();
    }
  };

  render() {
    const { newText, isColorPickerOpen, isTextareaFocused } = this.state;
    const { cardElement, card, listId, isOpen, boardId, toggleCardEditor } = this.props;
    if (!cardElement) {
      return null;
    }

    // Get number of checked and total checkboxes
    const checkboxes = findCheckboxes(newText);

    /*
    Create style of modal in order to not clip outside the edges no matter what device.
    */

    // Get dimensions of the card to calculate dimensions of cardModal.
    const boundingRect = cardElement.getBoundingClientRect();

    // Returns true if card is closer to right border than to the left
    const isCardNearRightBorder =
      window.innerWidth - boundingRect.right < boundingRect.left;

    // Check if the display is so thin that we need to trigger a centered, vertical layout
    // DO NOT CHANGE the number 550 without also changing related media-query in CardOptions.scss
    const isThinDisplay = window.innerWidth < 780;
    //console.log(window.innerWidth);

    // Position textarea at the same place as the card and position everything else away from closest edge
    const style = {
      content: {
        top: (window.innerHeight * 0.5) / 2,
        left: (window.innerWidth * 0.5) / 2,
       /* right: isCardNearRightBorder
          ? window.innerWidth - boundingRect.right
          : null,*/
       width: window.innerWidth * 0.5,
        height: window.innerHeight * 0.5,
        flexDirection: isCardNearRightBorder ? "row" : "row"
      }
    };

    // For layouts that are less wide than 550px, let the modal take up the entire width at the top of the screen
    const mobileStyle = {
      content: {
        flexDirection: "column",
        top: 3,
        left: 3,
        right: 3
      }
    };

    return (
      <Modal
        closeTimeoutMS={150}
        isOpen={isOpen}
        onRequestClose={this.handleRequestClose}
        contentLabel="Редактирование карточки"
        overlayClassName="modal-underlay"
        className="modal"
        style={isThinDisplay ? mobileStyle : style}
        includeDefaultStyles={false}
        onClick={this.handleRequestClose}
      >
        <div
          className="modal-textarea-wrapper"
          style={{
            minHeight: isThinDisplay ? "none" : boundingRect.height,
            //width: isThinDisplay ? "100%" : (window.innerWidth * 0.5) * 0.7,
            boxShadow: isTextareaFocused
              ? "0px 0px 3px 2px rgb(0, 180, 255)"
              : null,
          }}
        >
            <div className="card-color" style={{
                //width: isThinDisplay ? boundingRect.width : (window.innerWidth * 0.5) * 0.7,
                background: card.color
            }} />
          <Textarea
              style={{
                maxHeight: isThinDisplay ? "200px" : (window.innerHeight * 0.5) - 25,
              }}
            autoFocus
            useCacheForDOMMeasurements
            value={newText}
            onChange={this.handleChange}
            onKeyDown={this.handleKeyDown}
            className="modal-textarea"
            spellCheck={false}
            onFocus={() => this.setState({ isTextareaFocused: true })}
            onBlur={() => { this.setState({ isTextareaFocused: false });} }
          />
          {(card.date || checkboxes.total > 0) && (
            <CardBadges date={card.date} checkboxes={checkboxes} />
          )}
        </div>
        <CardOptions
          isColorPickerOpen={isColorPickerOpen}
          card={card}
          listId={listId}
          boardId={boardId}
          boundingRect={boundingRect}
          isCardNearRightBorder={isCardNearRightBorder}
          isThinDisplay={isThinDisplay}
          toggleColorPicker={this.toggleColorPicker}
        />
        <div style={{
          width: isThinDisplay ? window.innerWidth : '100%',
        }}
             className="action-buttons">
          <Button onClick={toggleCardEditor} className="cancel-button" color="default">
            Отмена
          </Button>
          <Button onClick={this.submitCard} variant="contained" className="save-button" color="primary">
            Сохранить
          </Button>
        </div>
      </Modal>
    );
  }
}

export default connect()(CardModal);
