import React from "react";
import BoardTitle from "./BoardTitle";
import BoardDeleter from "./BoardDeleter";
import "./BoardHeader.scss";
import BoardAddUser from "./BoardAddUser";
let rightSidebarIsOpen = false;
const BoardHeader = () => (
  <div className="board-header">
    <BoardTitle />
    <div className="board-header-right">
      {/*<ColorPicker />*/}
      <BoardAddUser/>
      {/*<div className="vertical-line" />*/}
      <BoardDeleter />
    </div>
  </div>
);

export default BoardHeader;
