import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Button, Wrapper, Menu, MenuItem } from "react-aria-menubutton";
import {Icon} from "@material-ui/core";
import "./BoardAddUser.scss";
import {socket} from "../../middleware/socketIOReduxMiddleware";
import {showSnack} from "react-redux-snackbar";

class BoardAddUser extends Component {
    static propTypes = {
        match: PropTypes.shape({
            params: PropTypes.shape({ boardId: PropTypes.string })
        }).isRequired,
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
    };

    handleSelection = () => {
        const { match, history } = this.props;
        const { boardId } = match.params;
        const { userId } = this.state
        // console.log(boardId, userId);
        // dispatch({ type: "ADD_USER_ON_BOARD", payload: { boardId, userId } });
        fetch("/api/checkUser", {
            method: "POST",
            body: JSON.stringify({ userId }),
            headers: { "Content-Type": "application/json" },
            credentials: "include"
        })
            .then(res => res.json())
            .then(json => {
                if (json.result) {
                        this.props.dispatch({type: 'ADD_USER_ON_BOARD', payload: {boardId, userId}});
                } else {
                    this.props.dispatch(showSnack('userDoesNotExsist', {
                        label: 'Пользователь с таким ID не найден',
                        timeout: 7000,
                        button: { label: 'OK' }
                    }));
                        // alert('Пользователь с таким ID не найден');
                }
            });

        // history.push("/");
    };

    handleInputChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render = () => (
        <Wrapper
            className="board-addUser-wrapper"
            onSelection={this.handleSelection}
        >
            <Button className="board-addUser-button">
                <div className="modal-icon">
                    <Icon>supervised_user_circle</Icon>
                </div>
                <div className="board-header-right-text">&nbsp;Добавить пользователя</div>
            </Button>
            <Menu className="board-addUser-menu">
                <div className="board-addUser-header"><input placeholder="ID пользователя" name="userId" onChange={this.handleInputChange}/></div>
                <MenuItem className="board-addUser-confirm">Добавить пользователя</MenuItem>
            </Menu>
        </Wrapper>
    );
}

export default withRouter(connect()(BoardAddUser));
