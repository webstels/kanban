import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Button, Wrapper, Menu, MenuItem } from "react-aria-menubutton";
import {Icon} from "@material-ui/core";
import "./BoardDeleter.scss";
import {socket} from "../../middleware/socketIOReduxMiddleware";

class BoardDeleter extends Component {
  static propTypes = {
    match: PropTypes.shape({
      params: PropTypes.shape({ boardId: PropTypes.string })
    }).isRequired,
    history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
    dispatch: PropTypes.func.isRequired
  };

  handleSelection = () => {
    const { match, history, dispatch } = this.props;
    const { boardId } = match.params;
    dispatch({ type: "DELETE_BOARD", payload: { boardId } });
      //socket.emit('DELETE_BOARD', {boardId});
    history.push("/");
  };

  render = () => (
    <Wrapper
      className="board-deleter-wrapper"
      onSelection={this.handleSelection}
    >
      <Button className="board-deleter-button">
        <div className="modal-icon">
          <Icon>delete</Icon>
        </div>
        <div className="board-header-right-text">&nbsp;Удалить доску</div>
      </Button>
      <Menu className="board-deleter-menu">
        <div className="board-deleter-header">Вы уверены?</div>
        <MenuItem className="board-deleter-confirm">Удалить</MenuItem>
      </Menu>
    </Wrapper>
  );
}

export default withRouter(connect()(BoardDeleter));
