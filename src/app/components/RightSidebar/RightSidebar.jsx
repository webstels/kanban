import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Textarea from "react-textarea-autosize";
import shortid from "shortid";
import "./RightSidebar.scss";
import {FaCogs} from 'react-icons/fa';
import {socket} from "../../middleware/socketIOReduxMiddleware";

class RightSidebar extends Component {
  static propTypes = {
    boardId: PropTypes.string.isRequired,
    // dispatch: PropTypes.func.isRequired
  };

  constructor() {
    super();

    this.state = {
      isOpen: false
    };
  }

  render = () => {
    const { isOpen } = this.state;
      return (
          <div className={ isOpen ? "rightSidebar open" : "rightSidebar" }>
            <div onClick={() => this.setState({isOpen: !isOpen})} className="openSidebar"><FaCogs/></div>
          </div>
      );
  };
}

export default connect()(RightSidebar);
