import io from 'socket.io-client';
import {showSnack, dismissSnack} from "react-redux-snackbar";

export const socket = io("kanbanspace.ru:80",
    {
        pingInterval: 10,
        pingTimeout: 10,
        autoConnect: true,
        transports: ["websocket", "polling"]
    });

export const EVENTS = [
    'PUT_BOARD_ID_IN_REDUX',
    'ADD_LIST',
    'ADD_USER_ON_BOARD',
    'ADD_CARD',
    'MOVE_LIST',
    'MOVE_CARD',
    'MOVE_CARD_ANOTHER_BOARD',
    'CHANGE_BOARD_TITLE',
    'CHANGE_LIST_TITLE',
    'CHANGE_CARD_TEXT',
    'CHANGE_CARD_DATE',
    'CHANGE_CARD_COLOR',
    'SET_WIP',
    'DELETE_LIST',
    'DELETE_BOARD',
    'DELETE_CARD',
    'UPDATE_DATA',
];

const socketIOReduxMiddleware = store => next => {
    socket.on('connect', () => {
        const {
            boardsById
        } = store.getState();
        for (const boardId in boardsById) {
            socket.emit('room', boardId);
        }
        EVENTS.forEach(event => {
            socket.on(event, payload => {
                if(socket.connected)
                {
                    store.dispatch({type: event, payload});
                }
            });
        });
    });
    socket.on('disconnect', () => {
        store.dispatch(showSnack('disconnect', {
            label: 'Потеряна связь с серверов, восстанавливаем....',
            timeout: 100000,
            //button: {label: 'OK'}
        }));
        console.log('disconnected!');
    });

    socket.on('reconnect', () => {
        store.dispatch({type: 'UPDATE_DATA', payload: {}});
        store.dispatch(dismissSnack('disconnect'));
        console.log('reconnected!');
    });

    return action => {
        next(action);
    }
}

export default socketIOReduxMiddleware;
