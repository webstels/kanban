import { denormalize, schema } from "normalizr";
import {socket} from "./socketIOReduxMiddleware";

// Persist the board to the database after almost every action.
const persistMiddleware = store => next => action => {
  next(action);
  const {
    user,
    boardsById,
    listsById,
    cardsById,
    rowsById,
    currentBoardId: boardId
  } = store.getState();


  // Nothing is persisted for guest users
  if (user) {
    if (action.type === "DELETE_BOARD" && !action.payload.is_socket) {
      fetch("/api/board", {
        method: "DELETE",
        body: JSON.stringify({ boardId }),
        headers: { "Content-Type": "application/json" },
        credentials: "include"
      });

      // All action-types that are not DELETE_BOARD or PUT_BOARD_ID_IN_REDUX are currently modifying a board in a way that should
      // be persisted to db. If other types of actions are added, this logic will get unwieldy.
    } else if (action.type !== "PUT_BOARD_ID_IN_REDUX" && action.type !== "UPDATE_DATA" && action.type !== "REFRESH_DATA") {
      // Transform the flattened board state structure into the tree-shaped structure that the db uses.
      const card = new schema.Entity("cardsById", {}, { idAttribute: "_id" });
      const row = new schema.Entity("rowsById", {}, { idAttribute: "_id" });
      const list = new schema.Entity(
        "listsById",
        { cards: [card], rows: [row] },
        { idAttribute: "_id" }
      );
      const board = new schema.Entity(
        "boardsById",
        { lists: [list] },
        { idAttribute: "_id" }
      );
      const entities = { cardsById, listsById, boardsById, rowsById };

      const boardData = denormalize(boardId, board, entities);

      // TODO: Provide warning message to user when put request doesn't work for whatever reason

      fetch("/api/board", {
        method: "PUT",
        body: JSON.stringify(boardData),
        headers: { "Content-Type": "application/json" },
        credentials: "include"
      }).then(_ => {
          socket.emit('UPDATE_DATA_FROM_SOCKET', {boardId: action.payload.boardId});
      });
      if(action.type === 'MOVE_CARD_ANOTHER_BOARD') {
        // Transform the flattened board state structure into the tree-shaped structure that the db uses.
        const card2 = new schema.Entity("cardsById", {}, { idAttribute: "_id" });
        const row2 = new schema.Entity("rowsById", {}, { idAttribute: "_id" });
        const list2 = new schema.Entity(
            "listsById",
            { cards: [card2], rows: [row2] },
            { idAttribute: "_id" }
        );
        const board2 = new schema.Entity(
            "boardsById",
            { lists: [list2] },
            { idAttribute: "_id" }
        );
        const entities2 = { cardsById, listsById, boardsById, rowsById };

        const boardData2 = denormalize(action.payload.destBoardID, board2, entities2);

        // TODO: Provide warning message to user when put request doesn't work for whatever reason

        fetch("/api/board", {
          method: "PUT",
          body: JSON.stringify(boardData2),
          headers: { "Content-Type": "application/json" },
          credentials: "include"
        }).then(_ => {
          socket.emit('UPDATE_DATA_FROM_SOCKET', {boardId: action.payload.destBoardID});
        });
        //socket.emit('UPDATE_DATA_FROM_SOCKET', {boardId: action.payload.destBoardID});
      }
    }
    if (action.type === "UPDATE_DATA") {
      fetch("/api/getAllData", {
        method: "GET",
        headers: { "Content-Type": "application/json" },
        credentials: "include"
      }).then(res => res.json()).then(result => {
          store.dispatch({
            type: 'REFRESH_DATA',
            payload: {boards: result.boardsById, lists: result.listsById, cards: result.cardsById}
          });
      });
    }
  }
};

export default persistMiddleware;
